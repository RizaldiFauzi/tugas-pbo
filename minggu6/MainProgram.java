package main;

import mahasiswa.Mahasiswa;

public class MainProgram {
	
	public static void main(String[] args) {
		Mahasiswa mhsobj = new Mahasiswa("A11.2020.12481", "Rizaldi Fauzi", 3.9, 24, "21-10-2002");
		System.out.println("Progdi : " + mhsobj.getProgdi("A11.2020.12454"));
		System.out.println("IPK status : " + mhsobj.ipkStatus());
		System.out.println("Tahun : " + mhsobj.getTahun());
		System.out.println("Tagihan SKS : " + mhsobj.getTagihanSks());
		System.out.println("Mhs Semester : " + mhsobj.getMhsSemester());
		System.out.println("Umur : " + mhsobj.getUmur());
	}
	
}
