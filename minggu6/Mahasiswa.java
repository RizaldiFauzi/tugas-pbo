package mahasiswa;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Mahasiswa {
	String nim;
	String nama;
	Double ipk;
	int sks;
	String tglLahir;//formatnya yyyy-mm-dd
	
	public Mahasiswa(String nim, String nama, Double ipk, int sks, String tglLahir) {
		this.nim = nim;
		this.nama = nama;
		this.ipk = ipk;
		this.sks = sks;
		this.tglLahir = tglLahir;
	}
	
	public String getProgdi(String progdi) {
		String ps="";
		progdi = progdi.split(".", -1)[0];
		if(progdi=="A11") {
			ps = "Teknik Informatika";
		}
		else if(progdi=="A12") {
			ps = "Sistem Informasi";
		}
		else if(progdi=="B11") {
			ps = "Manajemen";
		}
		else if(progdi=="B12") {
			ps = "Akuntansi";
		}
		else {
			ps = "Belum terdaftar";
		}
		
		return ps;
	}
	
	public String ipkStatus() {
		String stat="";
		/**
		 * jika 0 < ipk <=1 , maka stat = "Perlu usaha lebih"
		 * jika 1 < ipk <=2, maka stat = "Perlu segera menaikkan IPK"
		 * jika 2 < ipk <=2.75, maka stat = "Sedikit lagi"
		 * jika 2.75 < ipk <= 3, maka stat = "Memuaskan"
		 * jika 3 < ipk <= 3.5, maka stat = "Sangat Memuaskan
		 * jika 3.5 < ipk <=4, maka stat = "Dengan pujian"
		 * jika di atas 4, maka stat = "Range ipk di luar jalur" 
		 * 
		 * */
		
		if(ipk > 4) {
			stat = "Range ipk di luar jalur";
		}else if(ipk > 3.5 && ipk <= 4) {
			stat = "Dengan pujian";
		}else if(ipk > 3 && ipk <= 3.5) {
			stat = "Dengan pujian";
		}else if(ipk > 2.75 && ipk <= 3) {
			stat = "Sangat Memuaskan";
		}else if(ipk > 2 && ipk <= 2.75) {
			stat = "Sedikit lagi";
		}else if(ipk > 1 && ipk <= 2) {
			stat = "Perlu segera menaikkan IPK";
		}else if(ipk > 0 && ipk <= 1) {
			stat = "Perlu usaha lebih";
		}
		
		return stat;
	}
	
	public int getTahun() {
		int angkatan;
		/**
		 * cari angkatan dari nim yang diinput
		 * misal A11.2000.00001, maka akan mengembalikan 2000
		 * 
		 */
		
		angkatan = Integer.parseInt(this.nim.split(".")[1]);
		return angkatan;
	}
	
	public int getTagihanSks() {
		int perSks = 250000;
		
		/**
		 * cari berapa jumlah tagihan mahasiswa yang bersangkutan
		 * sks*perSks
		 */
		int tagihan = this.sks * perSks;
		return tagihan;
	}
	
	public int getMhsSemester() {
		int smt = 0;
		Calendar kld = Calendar.getInstance();
		int thnSkr = kld.get(Calendar.YEAR);
		
		/**
		 * Hitung mahasiswa sudah berapa semester kuliah
		 */
		smt = thnSkr - this.getTahun();
		return smt;
	}
	
	private Date dateFormatter(String pola, String tanggal) {
		Date tgl=null;
		SimpleDateFormat formatter = new SimpleDateFormat(pola);
		
		try {
			tgl = formatter.parse(tanggal);
		}
		catch(ParseException e) {
			e.printStackTrace();
		}
		return tgl;
	}
	
	public String getUmur() {
		String umur = "";
		
		/**
		 * Hitung umur di sini
		 */
		Calendar kld = Calendar.getInstance();
		Date umurDate = this.dateFormatter("dd-mm-yyyy", this.tglLahir);
		int lahir = umurDate.getYear();
		int thnSkr = kld.get(Calendar.YEAR);
		int hasilUmur = thnSkr - lahir;
		
		umur = hasilUmur + " Tahun";
		return umur;
		
	}
}

